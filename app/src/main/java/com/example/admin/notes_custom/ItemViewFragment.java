package com.example.admin.notes_custom;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */

//Fragment to display the note when clicked on
public class ItemViewFragment extends Fragment {

    private Note note;
    private TextView title;
    private TextView content;
    private FloatingActionButton delete;
    private FloatingActionButton edit;
    private MainActivity listener;
    private DBHelper db;

    public ItemViewFragment(){

    }
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity)
            listener = (MainActivity) context;
    }

    public void onCreate (Bundle b) {
        // Required empty public constructor
        super.onCreate(b);
        note = (Note)getArguments().getSerializable("openNote");
        Log.d("View_Fragment", "Note Object Received");
    }


    //Populating the elements in the layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_item_view, container, false);
        db = DBHelper.getInstance(getActivity());

        title = (TextView) rootView.findViewById(R.id.title);
        content = (TextView) rootView.findViewById(R.id.content);
        delete = (FloatingActionButton) rootView.findViewById(R.id.deleteNote);
        edit = (FloatingActionButton) rootView.findViewById(R.id.editNote);
        title.setText(note.getTitle());
        content.setText(note.getNote());

        //Define functionality for Edit button
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ItemViewFragment", "Edit Note Clicked");
                listener.editNote(note);
            }
        });

        //Define functionality for Delete buttons
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ItemViewFragment", "Delete Note Clicked");
                db.delete(note);
                Toast.makeText(getActivity(), "Noted Deleted Successfully!",
                        Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();
            }
        });

        return rootView;
    }

}
