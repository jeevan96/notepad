package com.example.admin.notes_custom;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Admin on 23-01-2018.
 */

//Adapter class
public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder>{
    private final ItemClickListener listener;
    ArrayList<Note> notes;

    public NoteAdapter(ArrayList<Note> notes, ItemClickListener listener){
        this.notes = notes;
        this.listener = listener;
    }

    //Function to return view holder
    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean attachToParentImmediately = false;

        View view = inflater.inflate(R.layout.note_layout, parent, attachToParentImmediately);
        NoteViewHolder viewHolder = new NoteViewHolder(view);
        return viewHolder;
    }

    //Interface to define ItemClick action
    public interface ItemClickListener {
        void onClickListItem(int clickedPosition);
    }

    //Function to bind data to View Holder
    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        holder.bind(notes.get(position));
    }

    //Function returns the number of items in the database
    @Override
    public int getItemCount() {
        return notes.size();
    }

    //Function to remove item (for swipe operation)
    public void removeItem(int position) {
        // notify the item removed by position
        notifyItemRemoved(position);
    }

    //ViewHolder class
    public class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private TextView text;
        private TextView datetime;
        public RelativeLayout viewBackground;
        public LinearLayout viewForeground;

        //Constructor function to get various TextViews and Layouts
        public NoteViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.dispTitle);
            text = (TextView) itemView.findViewById(R.id.dispText);
            datetime = (TextView) itemView.findViewById(R.id.datetime);
            viewForeground = (LinearLayout) itemView.findViewById(R.id.view_foreground);
            viewBackground = (RelativeLayout) itemView.findViewById(R.id.view_background);
            itemView.setOnClickListener(this);
        }

        //Function to bind the Note object to the view elements
        public void bind(Note tempNote){
            title.setText(tempNote.getTitle());
            text.setText((tempNote.getNote().length() > 60)?
                    tempNote.getNote().substring(0, 60) + "..."
                    : tempNote.getNote()); //Displays only first 60 characters of the note when the length of the note content is long
            datetime.setText(tempNote.getModified());
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            listener.onClickListItem(clickedPosition);
        }
    }
}
