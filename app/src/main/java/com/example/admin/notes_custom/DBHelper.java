package com.example.admin.notes_custom;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Admin on 22-01-2018.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper instance; //Singleton class
    private static final String DATABASE_NAME = "Notes";
    private String LOGGER = "SQLiteHelper";

    //Constructor method for Singleton Class
    public static DBHelper getInstance(Context context) {
        if(instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        Note.no_notes = getMaxNoteCount();
    }

    //Get the maximum value of note_id in table
    private int getMaxNoteCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT MAX(note_id) FROM Notes", null);
        if(cursor != null) {
            cursor.moveToFirst();
            return cursor.getInt(0);
        }
        else
            return 0;

    }

    //Database creation
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(LOGGER, "In OnCreate class");
        String sql = "CREATE TABLE " + DBContract.table_name +
                " (note_id INTEGER PRIMARY KEY, " +
                DBContract.title + " TEXT, " +
                DBContract.content + " TEXT, " +
                "created DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                "modified TEXT DEFAULT CURRENT_TIMESTAMP)";
        db.execSQL(sql);

    }

    //Creation of default notes on start-up (first time or when all notes are deleted)
    public void startUp() {
        Log.e(LOGGER, "Testing Database");
        if(getNoteCount() == 0) {
            Log.e(LOGGER, "In addition");
            Note x = new Note("What's Up?", "Hey! What's up? How you doing? This app will help you"
                     + " keep all your notes handy and safe");
            addNote(x);
            x = new Note("Secure on-device storage", "Notes here are stored securely on your device, avoiding illegal access of your confidential data! :)" +
                    "This is a demo text with more than 60 characters to prove that the main page displays only the first 60 characters");
            addNote(x);
        }
    }

    //Upgrading existing table
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(LOGGER, "Updating Database");
        String sql = "DROP TABLE IF EXISTS Notes";
        db.execSQL(sql);
        onCreate(db);
    }

    //Returns number of notes in the table
    public int getNoteCount() {
        Log.i(LOGGER, "Fetching note count");
        ArrayList<Note> notes = new ArrayList<>();
        String selectQuery = "SELECT * FROM Notes";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Log.i("No. of notes", ""+cursor.getCount());

        return cursor.getCount();
    }

    //Function to add note to database
    public void addNote(Note newNote){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Log.e(LOGGER,"Adding Data");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("note_id", newNote.getID());
        values.put("note_title", newNote.getTitle());
        values.put("note_content", newNote.getNote());
        values.put("modified", dateFormat.format(date));
        values.put("created", newNote.getCreated());
        Log.d(LOGGER, newNote.getCreated());
        db.delete("Notes", "note_id = " + newNote.getID(), null);
        db.insert("Notes", null, values);
        db.close();
    }

    //Function to delete a note from database
    public void delete(Note delNote){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Notes", "note_id = " + delNote.getID(), null);
    }

    //Function returns an ArrayList of 'Notes' in the database
    public ArrayList<Note> getAllNotes(){
        Log.i(LOGGER, "Fetching all notes");
        ArrayList<Note> notes = new ArrayList<>();
        String selectQuery = "SELECT * FROM Notes ORDER BY modified DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                Note note = new Note(Integer.parseInt(cursor.getString(cursor.getColumnIndex("note_id"))),
                                    cursor.getString(cursor.getColumnIndex("note_title")),
                                    cursor.getString(cursor.getColumnIndex("note_content")),
                                    cursor.getString(cursor.getColumnIndex("created")),
                                    cursor.getString(cursor.getColumnIndex("modified")));
                notes.add(note);
            } while(cursor.moveToNext());
        }
        return notes;
    }
}
