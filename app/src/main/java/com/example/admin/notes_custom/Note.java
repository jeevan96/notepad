package com.example.admin.notes_custom;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Admin on 22-01-2018.
 */

public class Note implements Serializable {
    public static int no_notes = 0;
    private int note_id;
    private String title;
    private String note;
    private String modified;
    private String created;

    //Constructor used when a new note is created
    public Note(String title, String note){
        this.note_id = ++no_notes;
        this.title = title;
        this.note = note;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        this.created = dateFormat.format(date);
        this.modified = dateFormat.format(date);
    }

    //Constructor used when a note is to be edited
    public Note(int note_id, String title, String note, String created, String modified){
        this.note_id = note_id;
        this.title = title;
        this.note = note;
        this.modified = modified;
        this.created = created;
    }

    //Setter and getter methods
    public int getID() {
        return note_id;
    }
    public String getTitle(){
        return title;
    }
    public String getNote() {
        return note;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setNote(String note){
        this.note = note;
    }
    public String getCreated() { return created; }
    public String getModified() { return modified; }
}
