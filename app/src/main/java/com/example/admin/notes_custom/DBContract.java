package com.example.admin.notes_custom;

import android.net.Uri;

/**
 * Created by Admin on 07-02-2018.
 */

//Class for defining database names
public class DBContract {
    public static final String table_name = "Notes";
    public static final String title = "note_title";
    public static final String content = "note_content";
    public static final String AUTHORITY = "com.example.admin.notes_custom";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_NOTES = "notes";
    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_NOTES).build();

}
