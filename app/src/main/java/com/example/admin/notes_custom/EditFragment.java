package com.example.admin.notes_custom;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */

//Redirected to this fragment for editing note or adding new note
public class EditFragment extends Fragment {

    private EditText title;
    private EditText text;
    private Note note;
    private FloatingActionButton addNote;
    private DBHelper db;
    private MainActivity listener;
    public EditFragment(){

    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity)
            listener = (MainActivity) context;
    }
    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        note = (Note)getArguments().getSerializable("note");

        Log.d("Edit_Fragment", "Note Object Received");

    }
    //Adding the note to the database when 'Add Note' button is clicked on
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit, container, false);

        db = DBHelper.getInstance(getActivity());

        title = (EditText) rootView.findViewById(R.id.enterTitle);
        text = (EditText) rootView.findViewById(R.id.enterNote);
        title.setText(note.getTitle());
        text.setText(note.getNote());

        addNote = (FloatingActionButton)rootView.findViewById(R.id.addNote);
        addNote.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                note.setNote(text.getText().toString());
                note.setTitle(title.getText().toString());
                if(!note.getNote().equals("") || !note.getTitle().equals("")){
                    Log.d("EditFragment","Adding new note");
                    db.addNote(note);

                }
                getFragmentManager().popBackStack();
            }
        });
        return rootView;

    }

}
