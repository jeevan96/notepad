package com.example.admin.notes_custom;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView listview;
    private ArrayList<Note> notes;
    private FloatingActionButton add;
    private DBHelper db;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private static final String LOGGER = "MainActivity";

    //Function loads default fragment with RecyclerView on start up
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        MainFragment mainFragment = new MainFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, mainFragment);
        fragmentTransaction.commit();
    }

    //Function creates a new note object and pass it to 'EditFragment'
    public void createNewNote(){
        Log.d(LOGGER, "Hey");
        Note newNote = new Note("", "");
        EditFragment editFragment = new EditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("note", newNote);
        editFragment.setArguments(bundle);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.container, editFragment);
        fragmentTransaction.commit();

    }

    //Function redirects to ItemViewFragment when a note is clicked on
    public void openViewFragment(Note openNote) {
        Log.d(LOGGER,"Opening view fragment");
        ItemViewFragment itemViewFragment = new ItemViewFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("openNote", openNote);
        itemViewFragment.setArguments(bundle);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.container, itemViewFragment);
        fragmentTransaction.commit();
    }

    //Function passes the Note object clicked on to EditFragment fragment when the note is the be edited
    public void editNote(Note editNote){
        EditFragment editFragment = new EditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("note", editNote);
        editFragment.setArguments(bundle);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.container, editFragment);
        fragmentTransaction.commit();
    }
}