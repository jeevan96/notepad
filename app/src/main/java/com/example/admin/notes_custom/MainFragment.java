package com.example.admin.notes_custom;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements NoteAdapter.ItemClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{

    private NoteAdapter noteAdapter;
    private ArrayList<Note> notes;
    private FloatingActionButton add;
    private DBHelper db;
    private RecyclerView recyclerView;
    private static final String LOGGER = "MainFragment";
    private MainActivity listener;
    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity)
            listener = (MainActivity) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.notes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(listener);

        //Layout manager for Recycler View
        recyclerView.setLayoutManager(layoutManager);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);



        db = DBHelper.getInstance(getActivity());
        db.startUp();
        add = (FloatingActionButton)rootView.findViewById(R.id.addNote);

        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(LOGGER, "onClick Button Clicked");
                listener.createNewNote();
            }
        });
        refreshList();
        return rootView;
    }

    //Function to refresh the list of notes displayed on the main fragment
    public void refreshList(){
        Log.d(LOGGER, "Refreshing List");
        notes = db.getAllNotes();

        //Populating the Recycler View
        noteAdapter = new NoteAdapter(notes, this);
        recyclerView.setAdapter(noteAdapter);
    }

    //Function to open the note contents when clicked on
    @Override
    public void onClickListItem(int clickedPosition) {
        listener.openViewFragment(notes.get(clickedPosition));

    }

    //Function to delete the note on swiping it on the recycler view
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof NoteAdapter.NoteViewHolder) {

            // remove the item from recycler view
            db.delete(notes.get(position));
            notes.remove(position);
            noteAdapter.removeItem(viewHolder.getAdapterPosition());

        }
    }
}