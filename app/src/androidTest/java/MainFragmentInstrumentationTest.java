import android.support.test.rule.ActivityTestRule;

import com.example.admin.notes_custom.MainActivity;
import com.example.admin.notes_custom.R;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.PositionAssertions.isCompletelyAbove;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class MainFragmentInstrumentationTest {
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    // Looks for an EditText with id = "R.id.etInput"
    // Types the text "Hello" into the EditText
    // Verifies the EditText has text "Hello"
    @Test
    public void validateButton() {
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.addNote)).check(matches(isDisplayed()));
    }
    @Test
    public void validateAdd() {
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(typeText("Validate Add Text"));
        onView(withId(R.id.enterNote)).perform(typeText("Body of validate text"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("Validate Add Text")).check(matches(isDisplayed()));
        onView(withText("Body of validate text")).check(matches(isDisplayed()));
    }
    @Test
    public void validateDeleteButton() {
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(typeText("Validate Delete Text"));
        onView(withId(R.id.enterNote)).perform(typeText("Body of validate delete text"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("Validate Delete Text")).check(matches(isDisplayed()));
        onView(withText("Validate Delete Text")).perform(click());
        onView(withId(R.id.deleteNote)).perform(click());
        onView(withText("Validate Delete Text")).check(doesNotExist());
    }
    @Test
    public void validateDeleteSwipe(){
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(typeText("Validate Delete Text"));
        onView(withId(R.id.enterNote)).perform(typeText("Body of validate delete text"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("Validate Delete Text")).check(matches(isDisplayed()));
        onView(withText("Validate Delete Text")).perform(swipeLeft());
        onView(withText("Validate Delete Text")).check(doesNotExist());
    }
    @Test
    public void validateEditTitle(){
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(typeText("Validate Edit Text 1"));
        onView(withId(R.id.enterNote)).perform(typeText("Body of validate edit text"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("Validate Edit Text 1")).check(matches(isDisplayed()));
        onView(withText("Validate Edit Text 1")).perform(click());
        onView(withId(R.id.editNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(replaceText("New edited string"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("New edited string")).check(matches(isDisplayed()));
        pressBack();
        onView(withText("New edited string")).check(matches(isDisplayed()));
    }
    @Test
    public void validateEditBody(){
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(typeText("Validate Edit Text 2"));
        onView(withId(R.id.enterNote)).perform(typeText("Body of validate edit text"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("Validate Edit Text 2")).check(matches(isDisplayed()));
        onView(withText("Validate Edit Text 2")).perform(click());
        onView(withId(R.id.editNote)).perform(click());
        onView(withId(R.id.enterNote)).perform(replaceText("New edited string 2"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("New edited string 2")).check(matches(isDisplayed()));
        pressBack();
        onView(withText("New edited string 2")).check(matches(isDisplayed()));
    }

    @Test
    public void validateOrder(){
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(typeText("Note 1"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.addNote)).perform(click());
        onView(withId(R.id.enterTitle)).perform(typeText("Note 2"));
        onView(withId(R.id.addNote)).perform(click());
        onView(withText("Note 2")).check(isCompletelyAbove(withText("Note 1")));
    }

}
